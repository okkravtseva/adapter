#include <iostream>
#include <string>
#include <memory>

using namespace std;


class Balloon
{
public:
	Balloon() = delete; // will never be generated
	Balloon(double volume, double mass, double molar) { Volume = volume, Mass = mass, Molar = molar; }
	double Volume;
	double Mass;
	double Molar; // Molar mass
	inline static const double R = 8.31441;
	double GetPressure(int T)
	{
		return (Molar * R * T) / Volume;
	}
	double AmountOfMatter()
	{
		return (Mass / Molar);
	}
	string ToString()
	{
		return ("Volume: " + to_string(Volume) + "; Mass: " + to_string(Mass) + "; Molar: " + to_string(Molar));
	}

};

class Adapter
{
public:
	Adapter() = delete; // will never be generated
	Adapter(double volume, double mass, double molar) {
		bl = make_unique<Balloon>(volume, mass, molar);
	}
	double CalculateDP(int T0, int dT)
	{
		return (bl->GetPressure(T0+dT) - bl->GetPressure(T0));
	}
	void ModifMass(double dm)
	{
		bl->Mass += dm;
	}
	inline string GetData() { return bl->ToString(); }

protected:
	unique_ptr<Balloon> bl;
};

int main()
{
	Adapter a(40, 10000, 0.0016); // 40L, Oxygen
	cout << a.GetData() << endl;
	cout << "Pressure difference between 10 and 20 Celsius: " << a.CalculateDP(283, 10) << endl; // 10C - > 20C
	cout << "increase mass by 500 gr\n";
	a.ModifMass(500);
	cout << a.GetData() << endl;
}